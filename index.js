var CytubeBot = require('./lib/cytubebot');

CytubeBot.Commands = require('./lib/chatcommands');

module.exports = CytubeBot;
