var http = require("http")
var https = require("https")
var logger = require("./logger")

var APIs = {

	// API call to anagramgenius.com
	"anagram": function (msg, apikey, callback) {
		var options = {
			host: "anagramgenius.com",
			path: "/server.php?" + "source_text=" + encodeURI(msg) + "&vulgar=1",
			timeout: 20
		}

		urlRetrieve(http, options, function (status, data) {
			data = data.match(/.*<span class=\"black-18\">'(.*)'<\/span>/)
			callback(data)
		})
	},

	// Attempts to get the socketurl from a cytube server
	"socketlookup": function (serverData, apiKeys, callback) {
		var excellentServerRegex = /^http(s)?:\/\/([\da-z\.-]+\.[a-z\.]{2,6})([\/\w \.-]*)*\:?(\d*)?\/?$/
		var matches = serverData.server.match(excellentServerRegex)
		var secure = matches[1] !== undefined
		var defaultPort = secure ? 443 : 80

		var options = {
			host: matches[2],
			port: matches[5] !== undefined ? matches[6] : defaultPort,
			path: "/socketconfig/" + serverData.room + ".json",
			timeout: 20
		}

		urlRetrieve(secure ? https : http, options, (res, data) => {
			// If we can't find the URL there's something wrong and
			// we should exit
			if (res !== 200) {
				logger.errlog.log(`!~~~! Error looking up Cytube server info ${res}`)
				process.exit(1)
			}

			var json = JSON.parse(data)
			var serverUrl

			for (const server of json.servers) {
				if (server.secure === true) {
					serverUrl = server.url
					break
				} else {
					serverUrl = server.url
				}
			}

			if (serverUrl) {
				console.log(`got url ${serverUrl}`)
				callback(serverUrl)
			} else {
				console.log(`got thing ${res}`)

				callback(null)
			}
		})
	},

	// API call for YouTube videos
	// Used to validate videos
	"youtubelookup": function (id, apiKey, callback) {
		var params = [
			"part=" + "id,contentDetails,status",
			"id=" + id,
			"key=" + apiKey
		].join("&")

		var options = {
			host: "www.googleapis.com",
			port: 443,
			path: "/youtube/v3/videos?" + params,
			method: "GET",
			dataType: "jsonp",
			timeout: 1000
		}

		urlRetrieve(https, options, function (status, data) {
			if (status !== 200) {
				callback(status, null)
				return
			}

			data = JSON.parse(data)
			if (data.pageInfo.totalResults !== 1) {
				callback("Video not found", null)
				return
			}

			var vidInfo = {
				id: data["items"][0]["id"],
				contentDetails: data["items"][0]["contentDetails"],
				status: data["items"][0]["status"]
			}

			callback(true, vidInfo)
		})
	}
}

var urlRetrieve = function (transport, options, callback) {
	var req = transport.request(options, function (res) {
		var buffer = ""
		res.setEncoding("utf-8")
		res.on("data", function (chunk) {
			buffer += chunk
		})
		res.on("end", function () {
			callback(res.statusCode, buffer)
		})
	})

	req.on('error', err => {
		console.error(`Something fucked up, ${err}`)

		callback(null)
	})

	req.end()
};

module.exports = {
	APIs: APIs,
	APICall: function (msg, type, apikey, callback) {
		if (type in this.APIs) {
			this.APIs[type](msg, apikey, callback)
		}
	},
	retrieve: urlRetrieve
}
