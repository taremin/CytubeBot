var api = require("./apiclient");
var commands = require("./chatcommands");
var fs = require("fs");
var botHandlers = require("./bothandlers");
var logger = require("./logger");
var perms = require("./permissions");
var RateLimiter = require("limiter").RateLimiter;
var TokenBucket = require("limiter").TokenBucket;
var utils = require("./utils");

module.exports = {
  init: function(cfg) {
    logger.syslog.log("Setting up bot");
    var bot = new CytubeBot(cfg);
    return bot;
  }
};

// Constructor
function CytubeBot(config) {
  var bot = this;

  // Begin config things

  // Cytube user info
  this.cytubeServer = config["cytubeServer"];
  this.flair = config["usemodflair"];
  this.pw = config["pw"];
  this.room = config["room"];
  this.roomPassword = config["roompassword"];
  this.username = config["username"];
  this.maxVideoLength = config["maxvideolength"];

  // Logging
  this.useLogger = config["enableLogging"];
  this.logger = logger;
  if (!this.useLogger) {
    this.turnOffLogging();
  }

  // APIs
  this.youtubeapi = config["youtubev3"];
  this.deleteIfBlockedIn = config["deleteIfBlockedIn"];
  if (this.deleteIfBlockedIn) {
    this.deleteIfBlockedIn = this.deleteIfBlockedIn.toUpperCase();
  }

  // End config things

  // Channel data
  this.userlist = [];
  this.playlist = [];
  this.previousUID = null;
  this.currentUID = null;
  this.currentMedia = {};
  this.leaderData = {
    currentTime: 0,
    paused: false
  };
  this.firstChangeMedia = true;
  this.channelEmotes = [];
  this.banlist = [];

  // Cooldown times / Rate Limiters
  this.addVideoLimiter = new TokenBucket(3, 1, "second", null);
  this.videoLookupLimiter = new RateLimiter(10, "second");
  this.timeSinceLastTalk = 0;
  this.timeSinceLastAnagram = 0;
  this.timeSinceLastTranslate = 0;
  this.timeSinceLastStatus = 0;

  // Bot data
  this.socket = this.getSocketURL(this.cytubeServer);
  this.startTime = new Date().getTime();
  this.isLeader = false;
  this.loggedIn = false;
  this.isPlaylistLocked = false;
  this.waitingFunctions = [];
  this.stats = {
    managing: false,
    muted: false,
    hybridMods: {},
    userLimit: false,
    userLimitNum: 10,
    disallow: []
  };

  this.readPersistentSettings(function(err) {
    if (err) {
      bot.writePersistentSettings();
    }

    bot.updatePersistentSettings();
  });

  // Add handlers
  if (this.socket) {
    botHandlers.addHandlers(this);
  }
}

// Sends a queue frame to the server
// type - the type of media ie. yt
// duration - the duration of the video, in seconds. Not really used atm
// temp - whether to add the media as temporary or not
// parsedLink - param used when $add is called
// pos - position to add
CytubeBot.prototype.addVideo = function(
  type,
  id,
  duration,
  temp,
  pos,
  parsedLink
) {
  var json = {};
  var bot = this;

  if (typeof pos === "undefined") {
    pos = "end";
  }

  if (typeof temp === "undefined" || temp === null) {
    temp = false;
  }

  if (!parsedLink) {
    json = {
      id: id,
      type: type,
      pos: pos,
      duration: 0,
      temp: temp
    };
    this.logger.cytubelog.log("!~~~! Sending queue frame for " + json["id"]);
    this.addVideoLimiter.removeTokens(1, function() {
      bot.socket.emit("queue", json);
    });
  } else {
    json = {
      id: parsedLink["id"],
      type: parsedLink["type"],
      pos: pos,
      duration: 0,
      temp: temp
    };
    this.logger.cytubelog.log("!~~~! Sending queue frame for " + json["id"]);
    this.addVideoLimiter.removeTokens(1, function() {
      bot.socket.emit("queue", json);
    });
  }
};

// Checks if the user has a given permission
// Returns true or false depending if they have that perm
// username - The user we're looking up
// rank - The rank the user should have
// permission - The permission to look up
// callback - The callback function
CytubeBot.prototype.checkPermission = function(
  username,
  rank,
  permission,
  callback
) {
  var permData = {
    username: username,
    rank: rank,
    permission: permission,
    callback: callback
  };

  perms.handle(this, "checkPermission", permData);
};

// Checks if users have too many items on the playlist
// And if so, delete them
CytubeBot.prototype.checkPlaylist = function() {
  var bot = this;
  if (!this.stats["userLimit"]) {
    return;
  }

  for (var i = 0; i < this.userlist.length; i++) {
    if (this.userlist[i]["addedMedia"].length > this.stats["userLimitNum"]) {
      // How many should we delete
      var numDelete =
        this.userlist[i]["addedMedia"].length - this.stats["userLimitNum"];
      var uids = bot.userlist[i]["addedMedia"].reverse();

      // Delete the videos
      for (var u = 0; u < numDelete; u++) {
        bot.deleteVideo(uids[u]);
      }
    }
  }
};

// Used by $poll
// Sends a newPoll frame to the server
// This will create a new poll
// poll - Poll object
CytubeBot.prototype.createPoll = function(poll) {
  this.socket.emit("newPoll", poll);
};

// Used by various methods
// Sends a delete frame to the server
// uid - The uid of the video to delete
CytubeBot.prototype.deleteVideo = function(uid) {
  if (typeof uid !== "undefined") {
    this.logger.cytubelog.log("!~~~! Sending delete frame for uid: " + uid);
    this.socket.emit("delete", uid);
  }
};

// Used by $disallow
// Users who are disallowed cannot use the bot
// user - The user to disallow/allow
// disallow - true/false true we disallow, false we allow
CytubeBot.prototype.disallowUser = function(user, disallow) {
  if (typeof user === "undefined") {
    return;
  }

  user = user.toLowerCase();
  var indexOfUser = this.stats["disallow"].lastIndexOf(user);

  if (disallow && indexOfUser === -1) {
    this.logger.syslog.log("!~~~! Disallowing: " + user);
    this.stats["disallow"].push(user);
  } else if (indexOfUser !== -1 && !disallow) {
    this.logger.syslog.log("!~~~! Allowing: " + user);
    this.stats["disallow"].splice(indexOfUser, 1);
  }

  this.writePersistentSettings();
};

// Used by $endpoll
// Sends a closePoll frame to the server
// Closes a poll
CytubeBot.prototype.endPoll = function() {
  this.socket.emit("closePoll");
};

// Gets the Cytube socketIO port
// server - The input from config.json
CytubeBot.prototype.getSocketURL = function(server) {
  var bot = this;
  var defaultReg = /(https?:\/\/)?(.*:\d*)/;
  var serverData = { server: server, room: this.room };
  this.logger.syslog.log("!~~~! Looking up socketIO info from server");

  api.APICall(serverData, "socketlookup", null, function(data) {
    if (data.match(defaultReg)) {
      bot.socket = require("socket.io-client")(data);
      botHandlers.addHandlers(bot);
      bot.start();
    } else {
      bot.logger.errlog.log("!~~~! Error getting socket.io URL");

      process.exit(1);
    }
  });
  return;
};

// Handles queue frams from the server
// data - the queue data
CytubeBot.prototype.handleAddMedia = function(data) {
  var bot = this;

  // See if we should delete this video right away
  // because that user has too many videos
  var pos = utils.handle(this, "findUser", data["item"]["queueby"]);
  if (typeof pos !== "undefined") {
    if (
      this.stats["userLimit"] &&
      this.userlist[pos]["addedMedia"].length >= this.stats["userLimitNum"]
    ) {
      this.sendPM(
        data["item"]["queueby"],
        "You have too many videos on the list"
      );
      this.deleteVideo(data["item"]["uid"]);
      return;
    }
    this.userlist[pos]["addedMedia"].push(data["item"]["uid"]);
  }

  if (this.playlist.length === 0) {
    this.playlist = [data["item"]];
  } else {
    var uid = data["after"];
    var index = utils.handle(this, "findIndexOfVideoFromUID", uid);
    this.logger.cytubelog.log("#~~# Adding video after: " + index);
    this.playlist.splice(index + 1, 0, data["item"]);
  }
};

// Handles changeMedia frames from the server
// If the bot is managing the playlist and the last video was not
// temporary it sends a delete frame.
// data - changeMedia data
CytubeBot.prototype.handleChangeMedia = function(data) {
  if (
    this.stats["managing"] &&
    this.loggedIn &&
    !this.firstChangeMedia &&
    this.playlist.length !== 0
  ) {
    var temp = true;
    var uid = this.previousUID;

    // Try our best to find out if the video is temp
    // If we get an exception it's because the media was deleted
    try {
      if (typeof uid !== "undefined") {
        temp = utils.handle(this, "getVideoFromUID", uid)["temp"];
      }
    } catch (e) {
      this.logger.cytubelog.log(
        "!~~~! Media deleted. handleChangeMedia lookup temp failed"
      );
    }

    if (typeof uid !== "undefined" && !temp) {
      this.deleteVideo(uid);
    }
  }
  this.currentMedia = data;
  this.firstChangeMedia = false;
  this.logger.cytubelog.log(
    "#~~# Current Video now " + this.currentMedia["title"]
  );
};

// Handles chatMsg frames from the server
// If the first character of the msg is $, we interpet it as a command.
// We ignore chat from before the bot was started, in order to avoid old
// commands.
CytubeBot.prototype.handleChatMsg = function(data, pm) {
  var bot = this;
  var username = data["username"];
  var msg = data["msg"];
  var time = data["time"];

  this.logger.cytubelog.log("!~~~! Chat Message: " + username + ": " + msg);

  var allowed = function() {
    if (bot.stats["disallow"].lastIndexOf(username) === -1) {
      return true;
    } else {
      bot.sendPM(username, "You're not allowed to use the bot");
      return false;
    }
  };

  // Ignore server messages
  if (username === "[server]") {
    return;
  }

  // Filter the message
  msg = utils.handle(this, "filterMsg", msg);
  if (!msg) {
    return;
  }

  // Try to avoid old commands from playback
  if (time < this.startTime) {
    return;
  }

  var handleCommand =
    msg.indexOf("$") === 0 &&
    username.toLowerCase() !== this.username.toLowerCase() &&
    this.loggedIn &&
    allowed();

  if (handleCommand) {
    return commands.handle(this, username, msg);
  }

  if (pm) {
    return;
  }

  // do something
};

// Handles delete frames from the server
// If there are no more videos in the playlist and
// we are managing, add a random video
// data - delete data
CytubeBot.prototype.handleDeleteMedia = function(data) {
  var uid = data["uid"];
  var index = utils.handle(this, "findIndexOfVideoFromUID", uid);

  if (typeof index !== "undefined") {
    this.logger.cytubelog.log("#~~~# Deleting media at index: " + index);

    var addedBy = utils.handle(this, "getVideoFromUID", uid)["queueby"];
    var pos = utils.handle(this, "findUser", addedBy);

    if (typeof pos !== "undefined") {
      // Remove the media from the user's addedMedia
      this.userlist[pos]["addedMedia"].splice(
        this.userlist[pos]["addedMedia"].indexOf(uid),
        1
      );
    }

    this.playlist.splice(index, 1);
    if (this.playlist.length === 0 && this.stats["managing"]) {
      this.addRandomVideos();
    }
  }
};

// Handles changes to the channel emote list
// emote - The emote object that has changed
CytubeBot.prototype.handleEmoteUpdate = function(emote) {
  return;
};

// Used by $permissions
// Handles a change in hybridMods or calls sendHybridModPermissions if no permission
// is given.
// permission - The permission we are changing, or undefined if there is none
// name - name of the user we want to change permissions for, or look up
CytubeBot.prototype.handleHybridModPermissionChange = function(
  permission,
  name
) {
  var permData = {
    permission: permission,
    name: name
  };

  perms.handle(this, "handleHybridModPermissionChange", permData);
};

// Handles login frame from the server
// data - The login data
CytubeBot.prototype.handleLogin = function(data) {
  var bot = this;
  if (!data["success"]) {
    return this.logger.syslog.log("!~~~! Failed to login");
  }

  // Be sure we have the correct capitalization
  // Some cytube functions require proper capitalization
  this.username = data["name"];
  this.socket.emit("requestPlaylist");

  this.logger.syslog.log("!~~~! Now handling commands");
  this.loggedIn = true;
};

// Handles mediaUpdate frames from the server
// If we are managing and the playlist only has one item
// and the video is about to end, we add a random video
CytubeBot.prototype.handleMediaUpdate = function(data) {
  this.leaderData["currentTime"] = data["currentTime"];
  this.leaderData["paused"] = data["paused"];

  var isLessThanSix = this.currentMedia["seconds"] - data["currentTime"] < 6;
  var playlistHasOneItem = this.playlist.length === 1;
  var doSomething =
    isLessThanSix && playlistHasOneItem && this.stats["managing"];

  if (doSomething) {
    this.logger.cytubelog.log(
      "Shit son, we gotta do something, the video is ending"
    );
    this.addRandomVideos();
  }
};

// Handles moveVideo frames from the server
// data - moveMedia data
CytubeBot.prototype.handleMoveMedia = function(data) {
  var from = data["from"];
  var after = data["after"];
  var fromIndex = utils.handle(this, "findIndexOfVideoFromUID", from);

  // Remove video
  var removedVideo = this.playlist.splice(fromIndex, 1);
  var afterIndex = utils.handle(this, "findIndexOfVideoFromUID", after);

  // And add it in the new position
  this.playlist.splice(afterIndex + 1, 0, removedVideo[0]);
  this.logger.cytubelog.log(
    "#~~~# Moving video from: " + fromIndex + " after " + afterIndex
  );
};

// Handles needPassword frames from the server
// needPasswords are sent when the room we are trying to join has a password
CytubeBot.prototype.handleNeedPassword = function() {
  if (this.roomPassword) {
    this.logger.cytubelog.log("!~~~! Room has password; sending password");
    this.socket.emit("channelPassword", this.roomPassword);
    this.roomPassword = null;
  } else {
    this.logger.cytubelog.log(
      "\n!~~~! No room password in config.json or password is wrong. Killing bot!\n"
    );
    process.exit(1);
  }
};

// Handles playlist frames from the server and validates the videos
// playlist - playlist data
CytubeBot.prototype.handlePlaylist = function(playlist) {
  var bot = this;
  for (var i = 0; i < this.userlist.length; i++) {
    this.userlist[i]["addedMedia"] = [];
  }

  this.playlist = playlist;
};

// Handles a removeEmote frame
// emote - The emote to be removed
CytubeBot.prototype.handleRemoveEmote = function(emote) {
  return;
};

// Handles setCurrent frames from the server
// This is a better way of handling the current media UID problem
// uid - UID of the current video
CytubeBot.prototype.handleSetCurrent = function(uid) {
  if (this.currentUID === null) {
    this.currentUID = uid;
    this.previousUID = uid;
  } else {
    this.previousUID = this.currentUID;
    this.currentUID = uid;
  }
};

// Handles the setLeader frame
// If it says we are leader, change isLeader
// name - The name of the leader
CytubeBot.prototype.handleSetLeader = function(name) {
  if (name.toLowerCase() === this.username.toLowerCase()) {
    this.isLeader = true;
    utils.handle(this, "loopThroughWaiting", "settime");
  } else {
    this.isLeader = false;
  }
};

// Handles setTemp frames from the server
// data - setTemp data
CytubeBot.prototype.handleSetTemp = function(data) {
  var temp = data["temp"];
  var uid = data["uid"];

  var index = utils.handle(this, "findIndexOfVideoFromUID", uid);

  if (typeof index === "undefined") {
    return this.logger.syslog.log("Error: handleSetTemp.index undefined.");
  }

  this.logger.cytubelog.log(
    "#~~~# Setting temp: " + temp + " on video at index " + index
  );
  this.playlist[index]["temp"] = temp;
};

// Handles setUserRank frames from the server
// data - setUserRank data
CytubeBot.prototype.handleSetUserRank = function(data) {
  for (var i = 0; i < this.userlist.length; i++) {
    if (this.userlist[i]["name"].toLowerCase() === data["name"].toLowerCase()) {
      this.userlist[i]["rank"] = data["rank"];
      this.logger.cytubelog.log(
        "!~~~! Setting rank: " + data["rank"] + " on " + data["name"]
      );
      break;
    }
  }
};

// Handles userLeave frames from the server
// user - userLeave data
CytubeBot.prototype.handleUserLeave = function(user) {
  var index = utils.handle(this, "findUser", user);
  if (typeof index !== "undefined") {
    this.userlist.splice(index, 1);
    this.logger.syslog.log("!~~~! Removed user: " + user);
    this.logger.syslog.log(
      "!~~~! Userlist has : " + this.userlist.length + " users"
    );
  }
};

// Handles userlist frames from the server
// userlistData - userlist data
CytubeBot.prototype.handleUserlist = function(userlistData) {
  this.userlist = userlistData;
};

// Reads the persistent settings or has the callback write the defaults
// callback - callback function, used to write the persistent settings
// if they don't exist
CytubeBot.prototype.readPersistentSettings = function(callback) {
  var bot = this;
  fs.readFile("persistent.json", function(err, data) {
    if (err) {
      return callback(true);
    } else {
      bot.stats = JSON.parse(data);
      bot.logger.syslog.log("!~~~! Read persistent settings");
      callback(false);
    }
  });
};

// Sends an assignLeader frame to the server
// user - Name of the user we're setting leader
CytubeBot.prototype.sendAssignLeader = function(user) {
  var rank = 0;

  try {
    rank = utils.handle(this, "getUser", this.username)["rank"];
  } catch (e) {} // Not in list

  // Sending assignLeader if not mod resuslts in being kicked
  if (rank < 2) {
    return 1;
  }

  this.logger.cytubelog.log("!~~~! Assigning leader to: " + user);
  this.socket.emit("assignLeader", {
    name: user
  });
};

// Sends a chatMsg frame to the server
// If we are using modflair it will try and send meta for it
// message - message to be sent
CytubeBot.prototype.sendChatMsg = function(message, override) {
  // Rank is used to send the modflair
  var rank = 0;

  // If we're muted or not done initializing, there's no point in continuing
  if ((this.stats["muted"] && !override) || !this.loggedIn) {
    return;
  }

  this.logger.cytubelog.log("!~~~! Sending chatMsg: " + message);
  rank = utils.handle(this, "getUser", this.username.toLowerCase());
  if (typeof rank !== "undefined") {
    rank = rank["rank"];
  }

  if (!this.flair) {
    this.socket.emit("chatMsg", {
      msg: message,
      meta: {}
    });
  } else {
    this.socket.emit("chatMsg", {
      msg: message,
      meta: {
        modflair: rank
      }
    });
  }
};

// Sends the hybridmod permissions for name
// name - name to send hybridmod permissions for
CytubeBot.prototype.sendHybridModPermissions = function(name) {
  if (name) {
    this.sendChatMsg(name + ": " + this.stats["hybridMods"][name]);
  }
};

// Sends a mediaUpdate frame
// time - The time the video is at, or the time we want to set
// paused - Should we pause the video
CytubeBot.prototype.sendMediaUpdate = function(time, paused) {
  if (typeof time !== "number" || typeof paused === "undefined") {
    return;
  } else if (!this.isLeader || !this.currentMedia) {
    return;
  }

  this.logger.cytubelog.log(
    "!~~~! Setting time on video to: " + time + " Paused: " + paused
  );

  this.socket.emit("mediaUpdate", {
    id: this.currentMedia["id"],
    currentTime: time,
    paused: paused,
    type: this.currentMedia["type"]
  });
};

// Used by $bump
// Sends a moveMedia frame to the server9
// from - The position of the video before
CytubeBot.prototype.sendMoveMedia = function(from) {
  if (typeof from !== "undefined") {
    this.logger.cytubelog.log("!~~~! Sending moveMedia frame for uid: " + from);
    this.socket.emit("moveMedia", {
      from: from,
      after: this.currentUID
    });
  }
};

// Sends a Private message
// to - The person we wish to send the message to
// msg - The message
CytubeBot.prototype.sendPM = function(to, msg) {
  if (!to) {
    return;
  }

  this.socket.emit("pm", {
    to: to,
    msg: msg,
    meta: {}
  });
};

// Sends a chatMsg with the status of the bot
// ie. is the bot muted or managing
CytubeBot.prototype.sendStatus = function() {
  var status = "[Muted: ";
  status += this.stats["muted"];
  status += "; Managing playlist: " + this.stats["managing"];
  status += "; Userlimit: " + this.stats["userLimit"];
  status += "; Userlimit Number: " + this.stats["userLimitNum"];
  status += "]";

  this.socket.emit("chatMsg", {
    msg: status,
    meta: {}
  });
};

// Sends an unban frame to the server
// json - unban data in the form {id: banId, name: username}
CytubeBot.prototype.sendUnban = function(json) {
  this.logger.cytubelog.log("!~~~! Sending unban for: " + JSON.stringify(json));
  this.socket.emit("unban", json);
};

// Used by $shuffle
// Emits a shufflePlaylist frame
CytubeBot.prototype.shufflePlaylist = function() {
  this.socket.emit("shufflePlaylist");
};

// Used to start the process of joining a channel
// Called after we have initialized the bot and set socket listeners
CytubeBot.prototype.start = function() {
  var bot = this;

  this.logger.syslog.log("Starting bot");
  this.socket.emit("initChannelCallbacks");
  this.socket.emit("joinChannel", {
    name: this.room
  });
  this.socket.emit("login", {
    name: this.username,
    pw: this.pw
  });
};

// Turns off log writing
CytubeBot.prototype.turnOffLogging = function() {
  this.logger.errlog.enabled = false;
  this.logger.cytubelog.enabled = false;
  this.logger.syslog.enabled = false;
  this.logger.errlog.close();
  this.logger.cytubelog.close();
  this.logger.syslog.close();
};

// Updates the persistent settings
CytubeBot.prototype.updatePersistentSettings = function() {
  var changed = false;
  if (!this.stats["hybridMods"]) {
    changed = true;
    this.stats["hybridMods"] = {};
  }
  if (typeof this.stats["userLimit"] === "undefined") {
    changed = true;
    this.stats["userLimit"] = false;
    this.stats["userLimitNum"] = 10;
  }
  if (typeof this.stats["disallow"] === "undefined") {
    changed = true;
    this.stats["disallow"] = {};
  }

  if (
    Object.prototype.toString.call(this.stats["disallow"]) === "[object Object]"
  ) {
    var tempDisallow = [];
    for (var key in this.stats["disallow"]) {
      if (this.stats["disallow"].hasOwnProperty(key)) {
        tempDisallow.push(key);
      }
    }
    this.stats["disallow"] = tempDisallow;
    changed = true;
  }

  if (changed) {
    this.writePersistentSettings();
  }
};

// Writes the persistent settings
// Used by various methods
CytubeBot.prototype.writePersistentSettings = function() {
  var bot = this;
  this.logger.syslog.log("!~~~! Writing persistent settings");
  var stringyJSON = JSON.stringify(this.stats);
  fs.writeFile("persistent.json", stringyJSON, function(err) {
    if (err) {
      bot.logger.errlog.log(err);
      process.exit(1);
    }
  });
};
